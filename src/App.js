import React from 'react'
import { connect } from 'react-redux'
import { ThemeProvider } from 'emotion-theming'
import { dark, light } from './theme/'
import Projects from './screens/Projects/'

/*
	true: light
	false: dark
*/

const App = ({ mode }) => 
	<ThemeProvider theme={mode ? light : dark}>
		<Projects/>
	</ThemeProvider>	

export default connect(state=>({
	mode: state.theme
}))(App)