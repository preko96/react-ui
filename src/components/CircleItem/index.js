import styled from 'react-emotion'

const CircleItem = styled.div`
	display: flex;
	justify-content: center;
	align-items: center;
	border-radius: 50%;
	cursor: pointer;
	height: 40px;
	width: 40px;
	background-color: ${props=>props.theme.main};
`

export default CircleItem