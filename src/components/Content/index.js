import styled from 'react-emotion'
import Flex from '../Flex/'

const Content = styled(Flex)`
	flex-direction: column;
	background-color: ${props=>props.theme.contentBackground};
	padding: ${props=>props.padder ? 20 : 0}px;
`

export default Content