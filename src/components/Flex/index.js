import styled from 'react-emotion'
const Flex = styled.div`
	display: flex;
	padding: 0 ${props=>props.padder ? '10px' : '0'};
	flex: ${props => props.flex};
	flex-direction: ${props => props.direction};
	align-items: ${props => props.align};
	justify-content: ${props => props.justify};
	flex-wrap: ${props => props.wrap};
`

Flex.defaultProps = {
	direction: 'row',
	align: 'flex-start',
	justify: 'flex-start',
	wrap: 'wrap',
	flex: 'initial'
}

export default Flex