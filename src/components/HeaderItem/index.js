import React from 'react'
import PropTypes from 'prop-types'
import styled from 'react-emotion'
import { Icon } from 'antd' 
import Flex from '../Flex/'
import TouchableOpacity from '../TouchableOpacity/'
import { Paragraph } from '../Text/'

const Styled = styled(Flex)`
	padding: 10px 20px;
	align-items: center;
	box-shadow: 0px 5px 0px 0px ${props=>props.active ?
		props.theme.activeHeaderItem : 
		'rgba(0, 0, 0, 0)'
	};
	${Paragraph} {
		font-size: 16px;
		line-height: 16px;
		margin-left: 6px;
		color: ${props=>props.active ?
			props.theme.activeHeaderItem : 
			props.theme.inactiveHeaderItem 
		};
	}
	.anticon {
		font-size: 30px;
		color: ${props=>props.active ?
			props.theme.activeHeaderItem : 
			props.theme.inactiveHeaderItem 
		};
	}
`

const HeaderItem = ({ icon, name, active, ...rest }) => 
	<TouchableOpacity {...rest}>
		<Styled active={active}>
			<Icon type={icon}/>
			<Paragraph>{name}</Paragraph>
		</Styled>
	</TouchableOpacity>

HeaderItem.propTypes = {
	icon: PropTypes.string,
	name: PropTypes.string,
	active: PropTypes.bool
}

export default HeaderItem