import React from 'react'
import styled from 'react-emotion'
import PropTypes from 'prop-types'
import Flex from '../Flex/'

const Item = ({ children, ...rest }) =>
	<Flex {...rest}>
		{ children }
	</Flex>

Item.propTypes = {
	children: PropTypes.node
}

export default styled(Item)``