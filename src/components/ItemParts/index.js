import Left from './left'
import Body from './body'
import Right from './right'

export {
	Left,
	Body,
	Right
}