import styled from 'react-emotion'
import Flex from '../Flex/'

const Left = styled(Flex)`
	flex: 1 1 0;
	align-self: center;
	justify-content: flex-start;
`

export default Left