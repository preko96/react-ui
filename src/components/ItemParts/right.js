import styled from 'react-emotion'
import Flex from '../Flex/'

const Right = styled(Flex)`
	flex: 1 1 0;
	align-self: center;
	justify-content: flex-end;
`

export default Right