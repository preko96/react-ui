import styled from 'react-emotion'

const TouchableFeedback = styled.div`
	transition: all ${props => props.duration}s;
	:active {
		opacity: 0.5;
	}
`

TouchableFeedback.defaultProps = {
	duration: 0.3
}

export default TouchableFeedback