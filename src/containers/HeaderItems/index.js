import React from 'react'
import PropTypes from 'prop-types'
import styled from 'react-emotion'
import Flex from '../../components/Flex/'
import HeaderItem from '../../components/HeaderItem/'

const HeaderItemsContainer = styled(Flex)``

class HeaderItems extends React.Component {

	state = { active: null }

	onClick = id => () => {
		this.setState({ active: id })
		if(this.props.onClick)
			this.props.onClick(id)
	}

	render() {
		const { items, renderItem } = this.props
		return(
			<HeaderItemsContainer>
				{
					items.map(({ icon, name, id, ...rest }, index)=>
						renderItem ?
							renderItem({ icon, name, id, ...rest }) :
							<HeaderItem
								active={this.state.active === id}
								key={index} 
								icon={icon} 
								name={name} 
								onClick={this.onClick(id)}
								id={id}
								{...rest}
							/>
					)
				}
			</HeaderItemsContainer>
		)
	}
}
	

HeaderItems.defaultProps = {
	items: [],
}

HeaderItems.propTypes = {
	items: PropTypes.arrayOf(
		PropTypes.shape({
			icon: PropTypes.string,
			name: PropTypes.string,
			id: PropTypes.string
		})
	),
	renderItem: PropTypes.func,
	onClick: PropTypes.func
}

export default HeaderItems