import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import store from './redux/store'
import App from './App.js'
import './ReactotronConfig'
import 'antd/dist/antd.css'

const EnhancedApp = () => 
	<Provider store={store}>
		<App/>
	</Provider>

ReactDOM.render(<EnhancedApp />, document.getElementById('root'))
