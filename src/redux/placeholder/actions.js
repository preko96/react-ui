import { PLACEHOLDER } from 'constants'

export const placeholder = () => ({
	type: PLACEHOLDER
})