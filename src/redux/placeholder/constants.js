import createReduxModule from '../../utils/createReduxModule'

const module = name => createReduxModule('Placeholder', name)

export const PLACEHOLDER = module('PLACEHOLDER')