import { fork, all } from 'redux-saga/effects'
import placeholder from './placeholder/saga'

const sagas = [
	placeholder
]

export default function* rootSaga() {
	yield all(sagas.map(saga => fork(saga)))
}
