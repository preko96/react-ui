import { combineReducers, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'
import createSagaMiddleware from 'redux-saga'
import ReactotronConfig from '../ReactotronConfig'
import reducers from './reducers'
import rootSaga from './sagas'

const sagaMonitor = ReactotronConfig.createSagaMonitor()
const sagaMiddleware = createSagaMiddleware({ sagaMonitor })
const middlewares = [thunk, sagaMiddleware]

const enhancer = compose(
	applyMiddleware(...middlewares),
	window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)

const store = ReactotronConfig.createStore(combineReducers({ ...reducers }), enhancer)
sagaMiddleware.run(rootSaga)

export default store
