import createReduxModule from '../../utils/createReduxModule'

const module = name => createReduxModule('Theme', name)

export const ON_SWITCH_THEME = module('ON_SWITCH_THEME')