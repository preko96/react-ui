import React from 'react'
import styled from 'react-emotion'
import { Flex, H1, Layout, Header, Content, Search, Paragraph, CircleItem, Item, Left, Body, Right } from '../../components/'
import { HeaderItems, ThemeSwitcher } from '../../containers/'

const items = [
	{
		icon: 'appstore',
		name: 'Projects',
		id: 'projects',
	},
	{
		icon: 'plus-square',
		name: 'Create',
		id: 'create',
	},
	{
		icon: 'upload',
		name: 'Import',
		id: 'import',
	}
]

const ThemeSwitcherWrapper = styled(Flex)`
	justify-content: flex-end;
	width: 100%;
	position: absolute;
	padding: 10px;
`

const ItemsContainer = styled.div`
	width: 1200px;
	max-width: 75%;
`

const Padding = styled.div`
	padding-left: ${props=>props.left || props.padding}px;
	padding-right: ${props=>props.right || props.padding}px;
	padding-top: ${props=>props.top || props.padding}px;
	padding-bottom: ${props=>props.bottom || props.padding}px;
`

const ItemWrapper = styled(Flex)`
	padding: 10px 10px;
	margin-bottom: 5px;
	justify-content: space-between;
	align-items: center;
	background-color: ${props=>props.theme.projectItemWrapper};
	flex: 1;
`

const ItemDetails = styled.section`
	${Paragraph}:first-of-type {
		color: ${props=>props.theme.text}
	}
	${Paragraph} {
		color: ${props=>props.theme.main}
	}
`

const ItemRight = styled(Flex)`
	${CircleItem}:not(:first-of-type) {
		margin-left: 10px;
		background-color: ${props=>props.theme.danger};
	}
`

const FavIndicator = styled(CircleItem)`
	background-color: ${props=>props.active ?
		props.theme.active :
		props.theme.inactive
	};
`

const ItemLeft = styled(Flex)`
	${FavIndicator}:first-of-type {
		margin-right: 15px;
	}
`

const ProjectItem = ({ active }) => 
	<ItemWrapper>
		<ItemLeft>
			<FavIndicator active={active}/>
			<ItemDetails>
				<Paragraph>
					react-ui
				</Paragraph>
				<Paragraph>
					/Users/mac/Desktop/react-ui
				</Paragraph>
			</ItemDetails>
		</ItemLeft>
		<ItemRight>
			<CircleItem/>
			<CircleItem/>
		</ItemRight>
	</ItemWrapper>

const TestLayout = styled(Layout)`
	${Item} {
		${Left} {
			background-color: red;
		}
	}
`

const It = () =>
	<Item padder>
		<Left>
			<div>AAAAAAAAAAAAAAAAAA</div>
		</Left>
		<Body>
			<div>BBBB</div>
		</Body>
		<Right>
			<div>CCCCCCCCC</div>
		</Right>
	</Item>

class Projects extends React.Component {

	state = { active: true }

	onSlide = () => this.setState(state=>({ active: !state.active }))

	render() {
		return(
			<TestLayout>
				<It/>
				<ThemeSwitcherWrapper>
					<ThemeSwitcher onClick={this.onSlide} active={this.state.active}/>
				</ThemeSwitcherWrapper>
				<Header direction='column' align='center' justify='center'>
					<H1>React Project Manager</H1>
					<HeaderItems
						items={items} 
						onClick={this.onClickHeaderItem}
					/>
				</Header>
				<Content padder align='center' flex='1'>
					<Search/>
					<Padding bottom={10}/>
					<ItemsContainer>
						<ProjectItem active/>
						<ProjectItem/>
					</ItemsContainer>
				</Content>
			</TestLayout>
		)
	}
}

export default Projects