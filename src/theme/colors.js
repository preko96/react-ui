import Color from 'color'

export default {
	white: Color('#FFF'),
	black: Color('#000000'),
	white90: Color('#FFF').alpha(0.9),
	black90: Color('#000000').alpha(0.9)
}

export const d = {
	main: Color('#3F4A56'),
	secondary: Color('#272E35'),
	danger: Color('#B84C4C'),
	active: Color('#4BB7B5'),
	inactive: Color('#CCC'),
	text: Color('#FFF').alpha(0.9),
}

export const l = {
	main: Color('#FAFAFA'),
	secondary: Color('#C1C5C8'),
	danger: Color('#FE2F2F'),
	active: Color('#FE6E2D'),
	inactive: Color('#CCC'),
	text: Color('#66727D'),
}