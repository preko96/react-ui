import basic, { d, l } from './colors'

export const dark = ({
	//main colors
	main: d.main.string(),
	secondary: d.secondary.string(),
	danger: d.danger.string(),
	active: d.active.string(),
	text: d.text.string(),

	toggleActive: d.active.string(),
	toggleInactive: d.inactive.string(),
	headerColor: d.main.string(),
	headerText: basic.white90.string(),
	activeHeaderItem: d.active.string(),
	inactiveHeaderItem: basic.white90.string(),
	contentBackground: d.secondary.string(),
	projectsSearch: basic.white90.string(),
	projectSearchShadow: d.active.alpha(0.2).string(),
	projectsSearchIcon: d.main.lighten(1).string(),
	projectItemWrapper: d.main.alpha(0.2).string(),
})

export const light  = ({
	main: l.main.string(),
	secondary: l.secondary.string(),
	danger: l.danger.string(),
	active: l.active.string(),
	text: l.text.string(),

	toggleActive: l.active.string(),
	toggleInactive: l.inactive.string(),
	headerColor: l.main.string(),
	headerText: l.text.string(),
	activeHeaderItem: l.active.string(),
	inactiveHeaderItem: l.text.string(),
	contentBackground: l.secondary.string(),
	projectsSearch: l.text.string(),
	projectSearchShadow: l.active.alpha(0.2).string(),
	projectsSearchIcon: l.main.lighten(1).string(),
	projectItemWrapper: l.main.alpha(0.2).string()
})